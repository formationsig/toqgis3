# Summary

<<<<<<< HEAD
## SIG Niveau 1 : Découverte et exploitation des SIG à l'échelle de l'opération
=======
## Atelier - Passer à QGIS v3.10.x
* [Préambule](/nouveautes_qgis_3.md#les-nouveautées-de-qgis-v310x)

* [1. Modifications générales](/nouveautes_qgis_3.md#1-modifications-générales)
*	[1.1 Format Qgz](/nouveautes_qgis_3.md#11-format-qgz)
  
*	[1.2 Table de stockage auxiliaire](/nouveautes_qgis_3.md#12-tables-de-stockage-auxiliaire)
  
*	[1.3 Création du profil utilisateur](/nouveautes_qgis_3.md#13-cr%C3%A9ation-dun-profil-utilisateur)
	
*	[1.4 Emplacement du fichier .qgis3](/nouveautes_qgis_3.md#14-emplacement-du-fichier-qgis3)
	
*	[1.5 Le format GeoPackage](/nouveautes_qgis_3.md#15-le-format-geopackage)
	
* [2. Modifications dans la fenêtre principale](nouveautes_qgis_3.md#2-modifications-dans-la-fen%C3%AAtre-principale)

	*	[2.1 Général](/nouveautes_qgis_3.md#21-g%C3%A9n%C3%A9ral)

	*	[2.2 Table attributaire](/nouveautes_qgis_3.md#22-table-attributaire)

	*	[2.3 Etiquettes](/nouveautes_qgis_3.md#23-%C3%A9tiquettes)

	*	[2.4 Styles et symboles](/nouveautes_qgis_3.md#24-styles-et-symboles)

	*	[2.5 Numérisation](/nouveautes_qgis_3.md#25-num%C3%A9risation)

* [3. Modifications dans le composeur](/nouveautes_qgis_3.md#3-modifications-dans-le-composeur)

	*	[3.1 Informations générales](/nouveautes_qgis_3.md#31-informations-g%C3%A9n%C3%A9rales)

	*	[3.2 Carte](/nouveautes_qgis_3.md#32-carte)

	*	[3.3 Flèche Nord](/nouveautes_qgis_3.md#33-fl%C3%A8che-nord)

	*	[3.4 Echelle](/nouveautes_qgis_3.md#34-echelle)

	*	[3.5 Légende](/nouveautes_qgis_3.md#35-l%C3%A9gende)

	*	[3.6 Export](/nouveautes_qgis_3.md#36-export)

* [4. Extensions utiles](/nouveautes_qgis_3.md#4-extensions-utiles)

	* [4.1 Bezier Editing](/nouveautes_qgis_3.md#41-bezier-editing)

	* [4.2 Données OpenStreetMap](/nouveautes_qgis_3.md#42-donn%C3%A9es-openstreetmap)

	* [4.3 Autosaver](/nouveautes_qgis_3.md#43-autosaver)

	* [4.4 Theme Selector](/nouveautes_qgis_3.md#44-theme-selector)

	* [4.5 French Locator Filter](/nouveautes_qgis_3.md#45-french-locator-filter)

	* [4.6 Visualist & Data Plotly](/nouveautes_qgis_3.md#46-visualist-data-plotly)

	* [4.7 Qfield & Qfield Sync](/nouveautes_qgis_3.md#47-qfield-et-qfield-sync)



* [Sources](/nouveautes_qgis_3.md#sources)
>>>>>>> 4e2c7f255d9699c17a2f328052961d4f006fe070
