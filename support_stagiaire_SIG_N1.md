> **Auteurs** : Frédéric Audouit, Sylvain Badey, Mathias Cunault, Millena Frouin, Denis Godignon, Emeline Le Goff, Anne Moreau, Sophie Oudry, Luc Sanson, Pierre Serafini, Perrine Toussaint
>
> **Contact** : Anne Moreau anne.moreau@inrap.fr



# Support de formation SIG 1 : découverte et exploitation des SIG à l'échelle de l'opération



## Introduction

 

Qu'est-ce qu'un SIG ?

Un système d'information géographique (SIG) permet de manipuler des **objets** qui ont une **représentation graphique** **dans un espace géographique défini** (RGF93 - Lambert 93 par exemple), associée à des attributs. Un attribut est une information qui décrit l'objet selon des caractéristiques définies : identifiant, longueur, largeur, nature, etc.

**Objet dans un SIG = forme géographique (géométrie) + données (attributs)**

Les SIG permettent donc la conception et l'utilisation de bases de données relationnelles spatiales. On passe ainsi de l'illustration au traitement de données : dès lors qu'on "dessine" dans un SIG, on crée de la donnée potentiellement exploitable à partir de constats et d'un raisonnement scientifique.

Il existe plusieurs logiciels de SIG, certains payants et d'autres libres. Le choix à l'Inrap a été fait d'utiliser QGIS, car c'est le logiciel libre le plus utilisé. Il est alimenté par une large communauté de développeurs et contributeurs et est régulièrement mis à jour.

 

# 1. Découverte et prise en main du logiciel

## 1.1. Interface de QGIS

 

Un **projet** : c’est votre espace de travail. Le projet contient des données qui sont stockées ailleurs (dans votre propre ordinateur ou sur un serveur externe ou encore sur le web).

 

L’**extension** .qgz est le format d’enregistrement par défaut à partir de la version 3.2. C’est un format compressé d’où le “z” à la fin. Il contient le projet en .qgs et d’autres ressources associées, notamment les tables de stockage auxiliaire, les symboles svg, les polices, les plugins, etc.

Les **données** importées sont soit des *images (raster)*, soit des *vecteurs*, soit des *tables*. Le projet vous permet de créer, manipuler, visualiser et mettre en forme vos données.

 

Les données affichées dans le projet ne sont pas incorporées à celui-ci mais seulement liées. Cela a plusieurs conséquences :

▪ Les mêmes données peuvent être affichées dans un autre projet avec une autre mise en forme, en revanche les modifications effectuées dans la couche (suppression par exemple) seront répercutées sur tous les projets. 

*Note : un peu comme une playlist musicale, une même chanson peut être dans 2 playlists différentes !*

▪ Si les données sont déplacées, il faudra indiquer le nouveau chemin d’accès à l’ouverture du projet

▪ Il faut différencier la sauvegarde du projet et celle des données.

 

Au premier démarrage, l’interface de QGIS se compose de différents éléments :

![canevas](images/01.png)

*NB : Si une barre d'outil ou un panneau ne sont pas présents, faire un clic droit dans la zone des barres d'outils et cocher le panneau ou la barre d'outil souhaités.*

 

\-    Le nom du projet en cours est indiqué en haut à gauche derrière la version du logiciel.

\-    Les barres d’outils et panneaux peuvent être affichés ou masqués avec un clic droit sur la barre d’outils en cochant/décochant leur intitulé.

 

Les *barres d’outils et panneaux* peuvent être déplacées en faisant un glisser/déposer à partir des séparateurs sous la forme de pointillés grisés verticaux ou horizontaux qui séparent les barres d’outils et panneaux.

 

Les barres d’outils permettent d’accéder via des icônes aux fonctionnalités les plus courantes du logiciel. On peut aussi y accéder par les menus.

Deux barres d’outils sont souvent placées :

▪ à gauche de l’interface : la Barre d’outils ‘Gestion des couches’

▪ à droite de l’interface : la Barre d’outils ‘Navigateur de carte’

 

Le *Panneau Couches* permet de visualiser les couches présentes dans le projet et d’interagir avec celles-ci, soit avec les icônes dans la partie supérieure du panneau, soit à l’aide d’un clic droit sur le nom de la couche.

 

Le *Panneau Explorateur de couches* permet de naviguer dans l’arborescence de votre ordinateur et éventuellement d’ajouter des données au projet. Il est aussi possible d’ajouter un dossier en favori “marque-pages” ; il est alors accessible en haut de l’arborescence de l’explorateur, sans avoir à naviguer de nouveau dans toute l’arborescence.

![marque-pages](images/02.png)

 

Le *Canevas* est le panneau central qui contient la carte composée de la superposition des couches présentes et affichées (cochées) dans le panneau couches.

Lorsque la souris est située dans le canevas, elle prend la forme d’une croix.

 

La *Barre d’état* contient des informations géographiques et permet d’interagir avec le canevas. De gauche à droite :

\-    Les coordonnées de la position de la souris (en forme de croix) dans le canevas dans le système de projection du projet.

\-    L’échelle d’affichage de la carte dans le canevas. L’échelle est modifiable soit en tapant directement le dénominateur (20 pour 1/20) soit grâce au menu déroulant en cliquant sur la petite flèche.

\-    La rotation permet d’indiquer en degré (de 0 à 180° et de 0 à 180°) une rotation pour l’affichage de la carte dans le canevas.

\-    La case à cocher “Rendu” permet d’activer (par défaut) ou de désactiver la mise à jour du rendu de la carte dans le canevas à chaque changement.

\-    L’icône SCR (Système de Coordonnées de Référence) indique le SCR utilisé pour l’affichage de la carte dans le projet et permet de le paramétrer.

###  

### Les systèmes de coordonnées de référence (SCR)

Pour plus de détails : voir la [fiche technique dédiée](https://formationsig.gitlab.io/fiches-techniques/priseenmain/04_projection_SCR.html). 

 

Passer de la surface sphérique (ellipsoïde) de la Terre (géoïde) à la surface plane d’une carte nécessite de recourir à une projection cartographique. Il s’agit de techniques mathématiques qui permettent la transformation de coordonnées géographiques en degrés, en coordonnées cartographiques métriques.

**Attention**, qui dit projection dit **déformation** : aussi, chaque pays possède son propre système de coordonnées (lui-même peut être décomposé en plusieurs SCR).

Dans le cadre de notre travail quotidien en France métropolitaine, il est fortement recommandé de travailler en RGF93/ Lambert 93 (EPSG:2154)

 

<img src="images/03.png" alt="img" style="zoom: 80%;" />



###  

### Naviguer dans la carte

 

La barre d’outils du Navigateur de carte permet de se déplacer dans le canevas

 

![img](images/04.png)

\-    La main permet de se déplacer dans la carte. Avec la souris il suffit de laisser la molette enfoncée pour se déplacer dans la carte.

\-    Les outils de zoom permettent de zoomer/dézoomer par simple clic ou mieux en traçant un rectangle sur la zone à afficher. L’utilisation de la molette de la souris permet un zoom/dézoom centré sur la position de la souris dans le canevas.

\-    En associant les zooms par rectangle avec les outils (zoom précédent) et (zoom suivant) la navigation est plus efficace et moins gourmande en ressource et donc en temps d’affichage.

\-    L’outil “Zoom sur la couche” permet d’afficher une couche en plein écran (ou, dans le panneau Couches faire un clic droit > Zoomer sur la couche).

\-    L’outil “Zoom sur l’emprise totale” permet d’afficher en plein écran toutes les couches (celles qui sont cochées dans le panneau Calques).

\-    Les outils de zoom avec un carré jaune permettent de se déplacer/ zoomer sur la sélection en cours.

\-    Enfin la double flèche permet de rafraîchir la vue/l’affichage.

 



##  

## 1.2. Paramétrage de QGIS

 

**Menu Projet > Propriétés > onglet Général**

### 1.2.1. Paramètres généraux

Il permet notamment de paramétrer le type d’enregistrement des chemins c'est-à-dire comment le projet fait pour accéder aux données.

\-    un chemin absolu commence toujours par la lettre du disque d’origine (C:// par exemple). Si la lettre du lecteur change (enregistrement sur une clé USB ou sur le NAS par exemple), le projet ne retrouvera pas le chemin d’accès aux données.

\-    un chemin relatif fait référence à la position relative des données par rapport à l’emplacement du projet. C’est le choix par défaut qui permet d’échanger simplement les projets et données afférentes. *Attention* : les données ne doivent pas changer de position dans l’arborescence.

 

Pour partager un projet il faut copier tout le dossier contenant le projet et les données.

Note : On donne la playlist et les morceaux de musique.

 

![img](images/05.png)

###  

### 1.2.2. Mesures et Affichage de coordonnées

Cet onglet général permet aussi de définir l’unité du canevas, c’est-à-dire l’unité de mesure utilisée dans le projet : bien préciser Mètre et Mètres carrés.

 

Ellipsoïde : choisir None/Planimetric : pour que les valeurs retournées soient des mesures cartésiennes (X, Y : en unité métrique).

### 1.2.3. Les systèmes de coordonnées de référence (SCR)

Pour éviter les erreurs de projection, les systèmes de coordonnées de référence peuvent aussi être paramétrés. 

 

Menu Préférences > Options > Onglet SCR

Pour les nouveaux projets, cocher “Utiliser le SCR par défaut” et, s’il n’y est pas, choisir l’EPSG 2154. 

Pour les couches, par défaut, choisir le SCR 2154 et quand une nouvelle couche est ajoutée ou créée, cocher “Demander le SCR”

 

![img](images/06.png)

Si vous devez utiliser des données extérieures ou que vous avez un doute dans le système de coordonnées de référence : recontacter le fournisseur de la donnée pour avoir le SCR d’origine de la couche ou appeler le topographe.

  

### 1.2.4. Onglet Couleurs

Il est possible d’ajouter des palettes de couleur déjà créées dans QGIS, afin de les retrouver facilement. 

Pour plus d’informations, voir la [fiche technique dédiée](https://formationsig.gitlab.io/fiches-techniques/vecteur_symbologie/04_palette_couleurs.html). 

### 1.2.5. Onglet Outils cartographiques

\-    Section *Identifier* : paramétrer la surbrillance (apparaît lorsque vous numérisez ou interroger une couche). Nous vous conseillons la couleur rouge et une transparence à 50% pour mieux voir l’édition. 

\-    Échelles prédéfinies : il est possible de paramétrer les échelles qui sont présentes dans la barre d’état du canevas de la carte.

 

### 1.2.6. Ajouter des extensions

De nombreux outils et algorithmes sont présents dans le cœur de QGIS, il est toutefois possible de les compléter au moyen [d’extensions](https://formationsig.gitlab.io/fiches-techniques/priseenmain/05_extensions.html) (ou plugins). 

 

Menu Extension > Installer/Gérer les extensions 

 

Pour les besoins de la formation, nous installerons : 

\-    Group Stats

\-    Bezier editing

\-    OpenLayers : pour cette dernière, qui est une version expérimentale, il faut au préalable, dans les paramètres des extension, cocher la case “Activer les extensions expérimentales”

\-    French Locator Filter : s’utilise avec la barre de recherche du projet principal, il suffit de saisir la localité ou l’adresse

 

##  

## 1.3. Barre d’outil des attributs

La Barre d’outil des Attributs contient des outils permettant d’identifier, de sélectionner et de manipuler les données spatiales et attributaires.

![img](images/07.png) 

 

### 1.3.1. Identifier

L’outil d’identification permet d’identifier, c’est-à-dire d’accéder aux attributs d’une entité géographique (vecteur) ou de la valeur d’un pixel (raster).

![img](images/08.png) 

#### 1.3.1.1. Identifier sur une couche vecteur

Pour une couche vecteur, sélectionner la couche à interroger dans le panneau Couches puis sélectionner l’outil et cliquer sur la carte pour accéder aux attributs de l’entité voulue. Un formulaire s’affiche pour présenter les attributs de l’entité identifiée.

![img](images/09.png)

 

Quand plusieurs couches sont superposées, l’outil propose une liste des couches à interroger.

Si plusieurs entités sont sélectionnées (parce qu’elles sont trop proches l’une de l’autre par exemple) à la place d’un formulaire c’est un panneau Identifier les résultats qui s’ouvre sous le panneau Couches avec une arborescence des couches, puis des entités, puis des attributs. Il suffit alors de sélectionner l’entité voulue dans l'arborescence et de cliquer sur l’icône pour afficher le formulaire.

 

![img](images/10.png)

Pour quitter l'outil *Identifier les résultats*, il suffit de cliquer sur la main ou l'outil de sélection. 

#### 1.3.1.2. Identifier un pixel d’une couche raster

Sur une couche raster il est possible d’identifier la valeur d’un pixel avec le même outil identifier ; Il faut également au préalable sélectionner la couche raster à interroger dans le panneau Couches. Le résultat s’affiche dans le panneau *Identifier les résultats*. 

 Sur un raster simple, nous n’avons que des informations de couleur RGB. Le seul raster qui fournit une information supplémentaire est le Modèle Numérique de Terrain dans lequel chaque pixel a une valeur d’altitude.

![img](images/11.png)

Dans l’exemple ci-dessus, le chiffre associé à Bande 1 (105.8) est l’altitude du pixel concerné. On note qu’il y a les coordonnées X et Y du pixel cliqué. 

 

#### 1.3.1.3. Outils mesurer

![img](images/12.png) 

Il est possible de mesurer (estimer) des distances, des aires et des angles avec les outils de mesures disponibles via l’icône de l’outil de mesure et son menu déroulant, en cliquant sur la petite flèche noire à côté de celui-ci. 

![img](images/13.png) 

 

Le relèvement correspond à l’angle par rapport au nord : de 180° Ouest à 180° Est. 

L’angle se mesure entre deux segments de ligne, de -180° à 180° en passant par 0, dans le sens horaire.

 

Après un clic sur l’outil désiré, il suffit de créer des sommets avec une succession de clics, les distance/aires/angles sont indiqués en temps réel. Un clic droit permet de finir la ligne/polygone/angle que l’on veut mesurer et de figer la mesure.

 

Attention, du fait des clics, cet outil reste imprécis. Si une entité a été numérisée sous forme de ligne ou de polygone, utilisez l'icône <img src="images/icone_identifier.png" alt="img" style="zoom:50%;" />.

 

Dans la fenêtre qui s’affiche (ou Menu Vue > Panneaux > Identifier les résultats), aller dans la section Dérivé : on obtient toutes les informations géométriques et géographiques de la donnée, “correctes” car on est dans un environnement géoréférencé ! 

 

### 1.3.2. Sélectionner

 

Les outils en rapport avec la sélection d’une entité comportent tous un carré jaune entouré d’un pointillé noir, accessibles dans la barre d’outils de sélection.

 

![![img](images/14.png) 

 

Pour sélectionner “spatialement” une ou plusieurs entités les outils de sélection sont regroupés dans l’outil sélection et son menu déroulant disponible en cliquant sur la flèche noire qui lui succède. Selon l’outil sélectionné, l’icône peut prendre une forme différente :

 

![img](images/15.png) 

 

<img src="images/16.png" alt="img" style="zoom:67%;" /> Le premier permet de sélectionner une entité en cliquant dessus ou plusieurs entités en traçant un rectangle de sélection.

<img src="images/17.png" alt="img" style="zoom:67%;" /> Le 2e permet de sélectionner en traçant un polygone, sommet par sommet en terminant par un clic droit pour le fermer.

<img src="images/18.png" alt="img" style="zoom:67%;" /> Le 3e permet de sélectionner à main levée en traçant la forme de la sélection, clic gauche pour commencer, faire le tracé, puis clic gauche pour finir.

<img src="images/19.png" alt="img" style="zoom:67%;" /> Le dernier permet une sélection radiale en cliquant sur le centre de la sélection et augmentant le rayon par un clic gauche maintenu. Une petite fenêtre apparaît et indique le rayon en mètres.

 

Attention, les sélections dans des couches différentes peuvent être cumulées ; il est donc crucial de désélectionner lorsque l’on a fini de travailler avec une sélection. 

<img src="images/20.png" alt="img" style="zoom:50%;" />L’icône suivante permet d’annuler la/les sélection(s) en cours.

## 1.4. La table attributaire

La structure de la table attributaire est accessible via les Propriétés de la couche > onglet : on peut y voir le format des champs (qu’on ne peut modifier), leurs noms (qu’on peut changer) et on peut ajouter des champs, mais on ne peut pas ajouter de valeurs.

Pour ajouter des valeurs (texte, chiffre), il faut ouvrir la table attributaire d’une couche vecteur. Elle est accessible via un clic droit sur la couche dans le panneau Couches > Ouvrir la table d’attributs ou par l’outil après sélection de la couche.

 

→ Ouvrir la table d’attributs de la couche DIR_Inrap.

 

Remarquer les alignements dans la table attributaire: à gauche = texte, à droite = numérique.

![img](images/21.png) 

 

Les colonnes / champs peuvent être triés par ordre croissant/décroissant en cliquant sur la flèche grisée à côté du nom des champs. 

 

**Attention** néanmoins, le tri dépend de vos données (texte VS numérique). Lorsque les nombres sont saisis comme du texte, le tri sera « bizarre » si le nombre de caractères n’est pas exactement le même ! ex : enregistrement à 3 chiffres, le premier enregistrement devra être en 001 et non seulement 1

ex : enregistrement à 4 chiffres, le premier enregistrement devra être en 0001 et non seulement 1, sinon le logiciel ne pourra pas faire le tri comme mentalement vous le feriez !

 

On peut sélectionner une entité en cliquant sur la première colonne grisée de la table servant d’identifiant de ligne (de 0 à n). Remarquer que l’entité sélectionnée dans la table est sélectionnée dans le canevas, elle est affichée en jaune. Chaque entité/enregistrement/ligne de la table d’attribut correspond à une entité géographique dans le canevas.

 

Pour **sélectionner plusieurs enregistrements** dans la table d’attributs il est possible d’utiliser les touches [Ctrl] et [Maj].

 

Pour **sélectionner toutes les entités** il faut cliquer sur la case grise à la jonction des colonnes et des lignes ou faire un [Ctrl]+[A].

 

Deux outils de navigation permettent de visualiser les sélections effectuées :

![img](images/22.png)permet de zoomer, c'est-à-dire d’afficher en “plein écran” la/les entité(s) sélectionnée(s). L’échelle est alors changée pour un affichage “au plus près”.

 

![img](images/23.png) permet de se déplacer sur la/les entité(s) sélectionnée(s) sans changer d’échelle.

Cet outil est très pratique pour naviguer sur son chantier archéologique d’une structure à l’autre tout en gardant la même échelle. ...et ainsi éviter l’effet “google maps” de perte des notions d’échelles et de distance.

 

D’autres outils sont propres à la table attributaire :

![img](images/24.png)permet de renvoyer les entités sélectionnées en haut de la table.

![img](images/25.png) permet d’inverser la sélection.

 

**Lors de l’ajout d’une nouvelle couche vecteur, l’ouverture de la table attributaire doit être un automatisme**. On ajoute une couche, on visualise le résultat sur le canevas et on ouvre la table attributaire !

  

## 1.5. Ajout de données géographiques

L’ajout de nouvelles couches se fait soit :

▪ via un glisser-déposer depuis l’explorateur QGIS 

▪ via le Menu Couche > Ajouter une couche

▪ via la Barre d’outils Contrôle des barres d’outils des couches

▪ via le Gestionnaire de sources de données : <img src="images/26.png" alt="img" style="zoom:67%;" />

 

<img src="images/27.png" alt="img" style="zoom:50%;" />

Pour Ajouter un objet déjà existant, l’icône contient un <img src="images/icone_plus.png" alt="img" style="zoom:67%;" />

Pour en créer un nouveau, l’icône comporte une <img src="images/icone_nouveau.png" alt="img" style="zoom:67%;" />

 

### 1.5.1. Les couches vecteur

 

L’icône <img src="images/icone_ajout_couche_vecteur.png" alt="img" style="zoom:67%;" /> permet d’ajouter une couche vecteur.

 

*On utilisera sans distinction fichier de forme / shapefile / couche vecteur.*

 

Un “Shape” (ou shapefile pour fichier de formes), est composé de plusieurs fichiers :

 

| .shp | obligatoire | stocke les   entités géographiques (les tracés vectoriels)   |
| ---- | ----------- | ------------------------------------------------------------ |
| .shx | obligatoire | stocke les index  des enregistrements du fichier ".shp" (sauvegarde) |
| .dbf | obligatoire | “data base file”  ; stocke les données attributaires  consultable sous  Excel, mais peu recommandé, peut corrompre l’intégrité du fichier |
| .prj | facultatif  | stocke la  projection associée  recommandé en vue  de la bonne intégration SIG du shapefile |
| .qml | facultatif  | stocke les  paramètres de style (couleurs, épaisseur des traits, polices des étiquettes…) |
| .qpj | facultatif  | stocke les  informations d’encodage                          |
| .sbx | facultatif  | stocke des index  n'existant qu'après une requête ou une jointure |
| .xml | facultatif  | stocke les  métadonnées relatives au shape                   |

 

#### Les problèmes éventuels d’encodage

 

A l'ouverture de votre couche vecteur il arrive parfois que les tables d’attributs contiennent des caractères bizarres et cela est dû à un problème d’encodage.

Les encodages à tester si vous avez des problèmes sont : System, UTF-8, windows-1252, latin1. L’encodage de la couche vecteur est à vérifier dans les Propriétés > onglet **Source**

 ![img](images/28.png)

 

Les couches ajoutées se positionnent au-dessus de celle qui est active (surlignée en grisé ou en bleu) dans le panneau couches.

 

Pour **connaître le nombre d’entités de la couch vecteur**, faire un clic droit sur la couche dans le panneau Couches > Montrer le décompte des entités.

 

Pour **afficher une couche en “plein écran”** dans le panneau couche, faire un clic droit sur la couche > Zoomer sur la couche.

 

**Définir une échelle de visibilité** 

clic-droit > Propriétés > onglet > visibilité selon l’échelle, 

ou clic-droit > Définir l’échelle de visibilité

 permet de définir les échelles minimales (la plus petite c’est à dire avec la vision la plus large) et maximale d’affichage de la couche (la plus grande c’est à dire le zoom le plus serré).

 

**Les autres onglets des Propriétés de couche vecteur**

Onglet Champs : accès à la structure de la table, notamment type et longueur.

 

Les autres onglets permettent d’accéder aux paramètres qui concernent la couche vecteur que ce soit pour son affichage (symbologie et étiquettes) ou la gestion de la structure des données attributaires (Champs et Jointure).

###  

### 1.5.2. Les couches raster

L’icône <img src="images/icone_ajout_couche_raster.png" alt="img" style="zoom:67%;" /> permet d’ajouter une couche raster.

 

*On utilisera sans distinction raster et image.*

 

La fenêtre de propriétés d’une couche raster comporte moins d’onglets que celle des vecteurs.

*Le SRTM (Shuttle Radar Topography Mission sont des fichiers topographiques fournis par les agences américaines de la NASA et la NGA) est un MNT (modèle numérique de terrain). Il dispose donc pour chaque pixel, de valeurs altimétriques (altitude en mètre).*

 

Les onglets **Source** (SCR) et **Information** (chemin du dossier) sont sensiblement les mêmes que pour les couches vecteurs.

 

L’onglet **Transparence** permet de jouer sur la transparence du raster.

Une bande de transparence du SRTM_France sur le Gris a été enregistrée ici par défaut.

 ![img](images/29.png)

 

Il est possible de récupérer une donnée d’altitude sur un MNT (ou MNS, MNE) en cliquant sur le pixel correspondant avec l’outil *Identifier les résultats* <img src="images/icone_identifier.png" alt="img" style="zoom:50%;" />

###  

### 1.5.3. Les couches WMS - WMTS

 

Le Web Map Service (WMS), le Web Map Tile Service (WMTS), le Web Feature Service (WFS) sont des protocoles fournissant des données géoréfencées par des URL. On parle souvent de flux de données. Ces différents services sont mis à disposition par des serveurs cartographiques.

**WMS** : *Web Map Service* fournit une image géoréférencée (rasters) à partir de différents serveurs de données (IGN, BRGM, DREAL, GeoBretagne, etc.)

**WMTS**: *Web Map Tile Service* fournit des images géoréfencées tuilées, et donc theoriquement un temps de chargement des sonnées plus rapide que le WMS.

**WFS** : *Web feature Service* permet de manipuler des objets géographiques (vecteurs : lignes, points, polygones)

Les informations générales sont à voir sur l'intranet Inrap à la page[ ](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign)[Se connecter aux webservices de l'IGN](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign).

#### 1.5.3.1. Paramétrer la connexion au flux BRGM

Vous n'avez à faire cette configuration que lors de la première connexion sur votre poste ou bien en cas de changement de version de QGIS. 

![img](images/30.png) 

 

●   Cliquer sur l'une des icônes ci-dessus en fonction du format (WMS/WMTS-WCS-WFS).

●  Cliquer sur le bouton [Nouveau]
![img](images/31.png) 

●   Saisir le nom et copier l'URL du flux dans la cellule dédiée ; valider avec [OK] 

 

#### 1.5.3.2. Paramétrer la connexion aux flux de l’IGN

●   Télécharger, dézipper et stocker les fichiers xml sur votre ordinateur (fichier accessible depuis[ ](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign)[l’article de l’intranet](https://intranet.inrap.fr/reseaux/sig/articles/se-connecter-aux-webservices-de-lign)). Attention! Il y a un fichier par format de données (WMTS, WFS).

●   Dans le panneau Gestionnaire de sources de données, cliquer sur [Charger], puis aller chercher le fichier .xml

●   Sélectionner tout ou partie des ressources puis [Importer]

![img](images/32.png) 

 

#### 1.5.3.3. Ajouter une couche depuis un serveur WMS

 

**Attention**, la connexion aux flux extérieurs est gourmande en ressources, il est préférable de ne pas être à l’échelle de la France entière avant d’ajouter un flux ! Pour le Scan25, se placer au 25000e par exemple.

 

●   Pour ajouter une couche, cliquer sur l'icône ou menu *Couche > Ajouter une couche > Ajouter une couche WMS/WMTS* ou Ctrl + Maj + W

●   Dans le menu déroulant, choisir le serveur puis cliquer sur [Connexion]

●   La liste des couches s’affiche

●   Une fois la couche sélectionnée, cliquer sur [Ajouter], puis [Fermer]

![img](images/33.png)


**N'oubliez pas de noter le copyright des données. Exemple : SCAN25® © IGN 2011**

###  

### 1.5.4. Ajouter une couche WFS

Qui dit flux WFS dit données vectorielles, dit table attributaire. Ainsi, pour récupérer en dur les données dans un dossier, on ne voudra pas faire une capture d’écran géoréférencée, mais bien récupérer les entités vectorielles et leurs informations attributaires. Aussi, on va créer un rectangle de sélection sur les entités que l’on souhaite récupérer, puis on part du flux et on va exporter les données et les enregistrer en une couche vecteur.

●   Zoomer sur l’emprise (clic-droit > zoomer sur la/les couches)

●   Charger le fichier XML (dossier stagiairesN1 > documentation > formation_outils > IGN_WFS_vecteur) et se connecter au flux **IGN - parcellaire** 

●   Récupérer les données parcellaires encadrant l’emprise du diagnostic > faire un carré de sélection autour de l’emprise > clic-droit > Exporter > Sauvegarder les entités sélectionnées sous (stocker le vecteur dans le dossier vecteur > FondsCarto_France ; le nommer parcellaire.shp)



### 1.5.5. Les couches tableur (CSV)

On peut parfois être amenés à projeter un ensemble de points comportant des coordonnées (X et Y dans le cadre d’un système métrique).

Cette projection de points sera temporaire et propre au projet. Aussi, on voudra pérenniser cette projection en la figeant dans une nouvelle couche vectorielle.

●   Ajouter couche de texte délimité <img src="images/34.png" alt="img" style="zoom:67%;" />

●   Projeter le jeu de points des centres Inrap (donnees_exercices > decouverte_QGIS > tableur > centresINRAP.csv) : X = X ; Y = Y (en EPSG 2154)

●   Pérenniser la couche en un vecteur dans le dossier decouverte_QGIS > vecteur (clic-droit > Exporter > Sauvegarder les entités sous) ; le nommer `centresINRAP.shp`

 

### 1.5.6. La gestion des couches (vecteur ou raster)

 

Il est possible d**’afficher/désafficher** une couche en cochant/décochant la case à cocher qui précède le nom de la couche dans le panneau Couches.

 

Autant les **fichiers vecteurs sont légers** (ce ne sont que les coordonnées des sommets et des tableaux de données) autant **les rasters peuvent être très lourds** (ce sont des images). Il est donc conseillé de décocher les rasters dont on n’a pas une utilité immédiate pour améliorer la vitesse d’affichage.

Enfin, dans le cas des flux, la connexion peut être instable.

 On peut aussi supprimer une couche via un clic droit sur le nom de la couche dans le panneau Couches > Supprimer ![img](images/icone_supprimer.png)

 *Supprimer une couche du projet ne la supprime pas de l’ordinateur, de la même façon supprimer une chanson d’une playlist ne la supprime pas de votre ordinateur !*

 

**La superposition des couches dans le canevas dépend de leur ordre dans le panneau Couche**.

 

Pour modifier l’ordre des couches il suffit de faire un glisser-déposer.

 Pour faire des **groupes** afin de réunir les couches et les afficher/désafficher en lot :

- Sélectionner les couches à regrouper

- Clic droit > grouper la sélection

- Clic droit > renommer

 ![img](images/35.png)

####  

#### Faire une capture d’écran géoréférencée

●   Commencer par se fixer à l’échelle souhaitée. 

●   Décocher les couches qui ne doivent pas apparaître sur la capture. 

●   Effectuer la capture, en allant dans le menu Projet > Importer/Exporter > Exporter au format image.

 

Dans l’idéal, avant de partir sur le terrain, nous vous préconisons de faire cela pour le BRGM et les fonds IGN (Scan1000, Scan25, BD parcellaire, BD ortho) et le cadastre napoléonien. 



## 1.6. Propriétés générales des couches vectorielles

Pour afficher la fenêtre de propriétés d’une couche il faut faire un clic-droit sur le nom de la couche dans le panneau Couches > **Propriétés** ou un double clic sur la couche.

 Comme les informations spatiales et attributaires sont liées dans le SIG, pour changer les couleurs et afficher les étiquettes, il faut savoir comment s’appelle le champ concerné, et donc **ouvrir la table attributaire**.

 

### 1.6.1. Étiquettes

L’onglet Étiquettes permet de paramétrer l’affichage des étiquettes sur les formes géographiques en fonction des données attributaires.

Dans un logiciel de SIG les étiquettes permettent d’afficher les attributs d’une entité et non pas du texte libre. Les étiquettes sont attachées aux entités et ne varient pas avec l’échelle de visualisation.



La boîte de dialogue de l’onglet Étiquettes est divisée en 3 parties :

#### Les paramètres généraux

-  Pas d’étiquettes/Montrer les étiquettes pour cette couche

- Choix de la colonne /du champ contenant les attributs à afficher en tant qu’étiquette

- Un aperçu du résultat

 

#### Les rubriques de paramétrage sous forme d’onglets verticaux

- ***Texte*** : style de texte c’est à dire Police, taille, couleur…

- *Formatage* : formatage du texte: caractère de retour à la ligne, alignement, ...

- ***Tampon*** : tampon autour du texte: auréole

- *Arrière-plan* : Affichage d’un fond ou d’un cadre

- *Ombre*

- ***Position*** : emplacement de l’étiquette par rapport à l’entité

- ***Rendu*** : paramétrage d’échelles de visibilité et de recouvrement entre étiquettes et par rappors aux objets, …

 

#### Les paramétrages qui changent selon la rubrique choisie

La rubrique **Position** change en fonction de la géométrie de la couche vecteur.

Par exemple, pour les couches de points, mettre un intervalle de 2 mm permet une meilleure lisibilité :

<img src="images/36.png" alt="img" style="zoom:67%;" />

 

La rubrique de paramétrage **Rendu** permet notamment de paramétrer :

▪ des échelles minimales et maximales de visibilité des étiquettes.

▪ de forcer l’affichage de toutes les étiquettes même quand elles se superposent.

 <img src="images/124.png" alt="img" style="zoom:67%;" />



### 1.6.2. Symbologie

<img src="images/37.png" alt="img" style="zoom:67%;" />L’onglet **Symbologie** permet de paramétrer l’affichage des formes géographiques en fonction des données attributaires.

 Les rubriques de la fenêtre **Symbologie** s’adaptent au type de géométrie de la couche vecteur, les options seront différentes selon que l’on a affaire à des points, des lignes ou des polygones.

 

La fenêtre de l’onglet **Symbologie** se “lit” de haut en bas puis de gauche à droite. Choisir le type de symbole sous la forme d’un menu déroulant :

●   **Symbole unique** pour afficher toutes les entités avec le même symbole.

●   **Symbole catégorisé** pour afficher un style différent aux entités selon les différentes occurrences (les valeurs uniques) d’un champ catégorisé c’est à dire avec une valeur qualitative (ex : des types de faits, des périodes, ...)

●   **Symbole gradué** pour afficher des styles différents (généralement un camaïeu d’une couleur) selon une valeur quantitative relative et continue (un taux, un rapport, une fréquence).

 ![img](images/38.png)

 

**Symbole unique : exemple pour une couche de polygone**

●   1) Cliquer sur Remplissage/symbole simple

●   2) Définir le **Remplissage** (le fond) et le **Trait** (le contour ou bordure) soit en cliquant sur la couleur pour accéder à une fenêtre de choix des couleurs : 4 onglets permettent de définir la couleur de 4 manières différentes : Palette de couleur, Roue chromatique, Aplats de couleur ou par pipette.

 

Il est possible d’accéder rapidement aux couleurs récentes et standards ainsi qu’aux options de pipette et de copier/coller des couleurs.

 

●   Définir le **style de remplissage : Continu** = aplats de couleur / **Pas de remplissage** = pas de fond / ou un style de trame

●   Définir le **style de trait** : Continu / Pas de ligne / ou des pointillés

●   Définir la largeur du trait (bordure) = épaisseur du trait

●   [Appliquer] pour visualiser le résultat dans le canevas et [OK] pour le valider.

 

#### Enregistrement par défaut de la symbologie

Il est possible de figer un style défini, lors de l’import d’une couche (Propriété > Symbologie > Style > Enregistrer par défaut) . une nouvelle extension a été ajoutée à côté du fichier .shp

#### Catégoriser

Catégoriser sert à trier les informations et leur appliquer un aspect visuel différent.

Exemple avec le vecteur ICPE_2154.shp (dans le dossier GeoRisques_BZH). Vous pouvez remarquer qu’il n’y a pas un symbole unique de point, mais une “catégorisation” qui a été effectuée sur le champ “famille”.

Une **catégorisation** s’effectue sur des champs au format texte. 

![img](images/39.png)



####  

#### Graduer

Pour les champs de format numérique, il est possible de graduer l’information et de lui appliquer un dégradé de couleur.

 

Le **format du champ contenant les valeurs est primordial** : numérique ou texte. C’est intrinsèque au fonctionnement d’un logiciel de SIG. Si on veut faire des dégradés de couleur, cercles proportionnels, des calculs ou des statistiques, il est impératif que le format du champ soit numérique.

 ![img](images/40.png)

**Pensez à enregistrer votre projet !** 



#  

# 2. Le projet archéologique

**Objectif** : préparer un projet QGIS avant de partir sur le terrain afin d’être autonome pour la mise à jour et la gestion des données, et la production de cartes durant l’opération.

 

## 2.1. Avant le décapage : faire ses cartes préparatoires

### 2.1.1. Fond de carte et organisation des couches

●   Créer un nouveau projet 

●   L’enregistrer avec le nom `Kersulec.qgz`

 

**Où l’enregistrer ?** Comme le projet va “puiser” dans les dossiers pour afficher les données, il doit être enregistré à la **racine** du dossier, ici dans donnees_exercices > D107698_PloneourLanvern_Kersulec

 

●   Ajouter les couches vecteur et les grouper (nom du groupe “données terrain”)

●   Ajouter les couches raster (nom du groupe “fonds carto”)

●   Ajouter les flux IGN : Scan 25 et BD Ortho 50 cm

●   Réaliser les captures d’écran géoréférencées et les ajouter au groupe “fonds carto”

menu Projet > Importer/Exporter > Exporter la carte au format image.

 

Le logiciel crée une image accompagnée d’un fichier de géoéférencement en .jpw ou .tfw (w pour world).

 

 

**Attention à l’ordre des couches pour que tout soit visible !**

 

*Exercice : Comment trouver les numéros de parcelles concernées par l’emprise ?* 

 

●   Sélection par le volet spatial : on sait où ça se trouve, mais on ne connaît pas son identité. 

○   Sélectionner les entités 

○   Ouvrir la table attributaire

○   Déplacer la sélection au sommet

○   Voir dans la table attributaire les attributs 

●   Sélection par le volet attributaire : on ne connaît pas la localisation d’une entité qui nous intéresse. 

○   Ouvrir la table attributaire

○   Chercher l’information par le filtre de champ

○   Zoomer dessus pour savoir où elle est

 ![img](images/41.png)

 

### 2.1.2. Créer une première carte

#### Option 1 : via le menu Vue

Objectif : créer une première carte simple en ajoutant une échelle et un nord

 

Menu Vue > Décorations : permet d’ajouter une flèche nord et une échelle graphique, il suffit ensuite d’exporter la carte au format image comme réalisé ci-dessus pour les flux.

 

#### Option 2 : via le module de Mise en page

 

Menu Projet > Nouvelle mise en page

●   Principe général : ajouter des objets au fur et à mesure en cliquant sur les icônes correspondantes puis les modifier des objets à l’aide des onglets situés à droite (les onglets s’adaptent lorsque l’on sélectionne l’objet à modifier)

●   Paramétrer la page

○   Clic droit dans la zone blanche 

○   Propriétés de la page

○   Choisir les dimensions et l’orientation de la page (attention aux gabarits de PAO)

●   Ajouter une carte

○   Soit par l’icône ajouter une carte ![img](images/42.png)

○   Soit par le menu Ajouter un objet > Ajouter carte ![img](images/43.png)

 

●   Propriétés de la carte

○   Echelle d’affichage

○   Plein d’autres options abordées plus tard

●   Ajouter une flèche nord

●   Ajouter une échelle graphique

○   Sélectionner le nombre de segments

○   Sélectionner la longueur des segments 

●   Exporter sa carte

○   Différents formats : image, svg, pdf

![img](images/44.png) 

##  

## 2.2. Importer et vérifier les données fournies par les topographes

**Préambule : ne pas altérer la donnée brute !**

On ne travaille jamais sur les données topo qui constituent la donnée initiale. Si des corrections sont à apporter, elles se feront sur une copie de ces données topo, copie qui sera alors un document de travail transitoire, ou directement sur la couche finale pour les plus aguerris. Ainsi le retour aux données topo initiales est toujours possible en cas de perte de données ou de mauvaises manipulations, cause beaucoup plus fréquente !

### 2.2.1. Alimenter le projet avec les données

#### Option A : les données sont déjà compilées par le topo

Passer à l’étape 2.3 de géoréférencement si besoin

 

#### Option B : les données ne sont pas compilées

On travaille à partir de couches vecteur vierges mais déjà structurées, exemple avec la couche poly qui contiendra les vestiges : elle a déjà un champ *numpoly* et un champ *interpret*. La base de données est donc vierge. 

 

##### Etape 1 : Importer les vecteurs vierges de l’opération Importer les vecteurs : point, log, axe et ptsTopo à importer.

##### Etape 2 : Création manuelle des vecteurs poly et ouverture

●   Menu Couche > Créer une couche > Nouvelle couche shapefile

![img](images/45.png)

 

●   Bien lire les fenêtres de haut en bas ! 

 

●   Paramétrer les informations du fichier : 

○   Indiquer le **chemin complet et le nom du fichier vecteur shapefile**: cliquer sur les 3 points ![img](images/46.png) *attention à ne pas saisir le seul nom du fichier dans la case blanche*

○   Codage du fichier : laisser par défaut ou passer en UTF-8 (pas important au moment de la création d’une couche)

○   Type de géométrie : choisir entre Point, Polyligne ou Polygone

○   Pas de dimensions supplémentaires

○   Attention au SCR !!

 

●   **Créer la structure de la table** avec les champs

○   Nom du champ : ne pas dépasser 10 caractères et surtout **NI caractères spéciaux ou accentués NI espaces**

○   **Type de champ** : choisir entre Texte, Nombre entier, Nombre décimal ou date. Attention !! Le type de champ ne peut pas être modifié par la suite !!

○   **Longueur du champ** : le nombre de caractères autorisés pour les enregistrements (on peut aller jusqu’à 254). Dans le cas de champ de type nombre décimal, il faut ajouter la *précision*, c’est-à-dire le nombre de chiffres après la virgule

 

Pour la couche poly, on utilisera les champs suivants : 

```
"numpoly" (texte, 5)
"typoly" (texte, 5)
"interpret" (texte, 20)
"datedebut" (entier, 10)
"datefin" (entier, 10)
```

 

Pour la couche ouverture, on utilisera les champs suivants : 

```
numouvert" (texte, 5)
"typouvert" (texte, 20)
```

 

##### Etape 3 : Import d’un levé topographique simple 

**Cas de figure où :  **

- les données fournies par le topo sont déjà en vecteur

- les noms des champs sont identiques entre la couche vierge et la couche topo

Levé topo du 3 juillet 2014

 

●   Charger les couches vecteurs fournies par le topo

●   Les vérifier rapidement : 

○   Est-ce que tout a été enregistré ?

○   Les entités ont-elles leur bon numéro ? Afficher les étiquettes des numéros de vestige

○   Est-ce qu’il y a des doublons ? ou des manques ?

○   Est-ce qu’il y a des problèmes d’encodage ?

●   Procéder à d’éventuelles corrections

●   Alimenter les couches vecteur vierges de l’opération en faisant des copier/coller. Attention, cette étape ne fonctionne que si les noms et types de champs sont identiques entre la couche topo et la couche d’arrivée (couche vierge ici) ! Il faut donc **vérifier les noms des champs** au préalable. 

○   **Sélectionner** toutes les entités de la couche topo ouverture avec Ctrl + A ou en cliquant sur le rectangle en haut à gauche de la table attributaire

○   **Copier** les entités avec Ctrl + C

○   Ouvrir la table attributaire de la couche vierge D107698_ouverture 

○   La passer en mode édition

 

**Quand vous passez en mode édition, attention à vos manipulations !!**

 

○   **Coller** les entités avec Ctrl + V

○   Vérifier que le nombre d’entités collées correspond bien au nombre d’entités copiées

○   **Enregistrer** les modifications sur la couche vierge D107698_ouverture

○   Quitter le mode édition

●   Il est important de **garder l’historique** des levés et pouvoir revenir à la donnée d’un levé en particulier : pour cela nous allons ajouter un champ Date qui contiendra la date du levé

○   Dans la table attributaire de D107698_ouverture, cliquer sur l’icone Ajouter un champ ![img](images/47.png)

○   **Créer** le champ “date” et choisir le format Date 

○   **Mettre à jour** le champ avec la date du levé, qui doit être écrite “à l’anglo-saxonne” : ‘2014-07-03’ (attention aux guillemets simples/apostrophes avant et après, et au trait d’union entre les éléments)

![img](images/48.png) 

 

![img](images/49.png) 

 

○   **Vérifier** ce que l’on a fait

○   **Enregistrer** la couche

○   **Quitter le mode édition**

 

●   Reproduire les manipulations sur les couches suivantes : 

○   `D107968_ptsTopo_20140703` *vers* `D107968_ptsTopo`

○   `D107968_poly_20140703` *vers* `D107698_poly`

 

#####  

##### Etape 4 : import de couches n’ayant pas les mêmes noms de champs

**Cas de figure où** : 
- les données fournies par le topo sont déjà en format vecteur

- les noms des champs NE SONT PAS IDENTIQUES entre la couche vierge et la couche topo

Levé topo du 10 juillet 2014

 

●   Charger les couches correspondant au relevé topo et les réunir dans un groupe 20140710 

●   Vérifier les champs de la couche D107698_ouverture_20140710 : le nom des champs ne correspond pas au nom des champs de la couche D107698_ouverture

●   Modifier le nom des champs de départ

○   Propriétés de la couche D107698_ouvertures_20140710 > rubrique Champs 

○   Passer en mode édition

○   Renommer les champs num en ‘‘numouvert’’ et type en ‘‘typouvert’’ par un double clic sur les valeurs à changer

○   Appliquer

○   Quitter le mode édition - fenêtre d’enregistrement apparaît

●   Réaliser le copier-coller des ouvertures de ce levé comme à l’étape 3, sans oublier de compléter le champ Date pour les données collées

●   Répéter les manipulations pour les couches D107698_poly et D107698_point

 

##### Etape 5 : import d’une couche de points csv

**Cas de figure où le relevé n’a pas pu être traité par le topographe et où seul une liste de points est fournie (format csv)**

 

Levé du 17 juillet 

 

La couche de points en .csv contient à minima les coordonnées X, Y, Z et l’identifiant de chaque point. 

On commence par [importer la couche de points qui deviendra géoréférencée](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/07_couche_points_csv.html) ; puis on dessinera les entités. 

 

●   Outil Ajouter une couche de texte délimité ![img](images/50.png)

(si vous ne l'avez pas, cet outil est dans la Barre d'outils "Gestion des couches") ![img](images/51.png)

 

1. Sélectionner le fichier .csv à importer. Modifier l'encodage si nécessaire.
2. Choisir le délimiteur à utiliser : le plus souvent, ce sera virgule ou point-virgule. Le tableau en bas de la fenêtre permet de visualiser le résultat.
3. Cocher si votre 1ere ligne correspond à l'entête du champ.
4. Définition de la géométrie : point. Indiquer quel champ correspond aux coordonnées X et quel champ correspond aux coordonnées Y.

**Attention**, le fichier qui apparaît dans le panneau des Couches n'est pas une couche vecteur, seulement un import du fichier csv !  Vous pouvez alors si besoin l'enregistrer en format vectoriel :

Clic droit sur la couche *Exporter > Sauvegarder les entités sous* `D107698_PtsTopo_20140717.shp`

### 2.2.2. Première approche du dessin

Avec ce dernier levé topo, les tranchées ne sont pas toutes dessinées : à nous de le faire.

Plusieurs étapes : identifier les numéros des points topo pour savoir dans quel ordre dessiner ; activer l’accrochage (= magnétisme) sur les points ; dessiner. 

 

●   **Identifier les points topo**

○   Catégoriser par type de point pour faire la différence entre isolat, fait, tranchée, etc. Décocher les couches dont on n’a pas besoin

○   Ajouter une étiquette sur le champ numero

 

●   **Paramétrer l’accrochage**

○   Cliquer sur l’icône en forme d’aimant 

○   Ouvrir la configuration avancée 

![img](images/52.png)

 

○   Dans la fenêtre qui s’est ouverte, choisir la couche qui nous intéresse

![img](images/53.png)

 

○   Activer l’accrochage sur la couche D107698_pts_topo_20140717 avec 1 m de tolérance

 

●   **Commencer le dessin**

○   Passer en mode édition sur la couche D107698_ouverture

○   Cliquer sur l’icône **Ajouter une entité polygonale** ![img](images/54.png)

○   Relier les points avec un clic gauche sur chaque point

○   Terminer sur l’avant-dernier point avec un clic droit

○   Remplir les attributs de l’entité dans la fenêtre qui s’affiche

 

●   **Corriger les éventuelles erreurs de tracé**

○   Au moyen de l’outil de noeuds ![img](images/55.png)

○   Un cercle rouge apparaît pour signaler le point sélectionné

○   Le **déplacement** du sommet (ou d’un segment) se fait en cliquant dessus, puis en cliquant sur la position finale (pas de maintien du clic enfoncé)

○   Pour **supprimer** : un clic et touche suppr

○   Possibilité **d’ajouter** un sommet en se plaçant à mi-segment et en cliquant sur le + gris qui apparît

○   Possibilité de travailler sur plusieurs couches en même temps (intéressant pour respecter les règles de topologie) à partir du menu déroulant de l’outil. Attention, c’est tout de même risqué !

 

### 2.2.3. Nettoyage de la table attributaire

Doublons spatiaux et numériques, manques

## 2.3. Géoréférencement et numérisation (dessin)

 

### 2.3.1. Géoréférencement

#### Concepts

**Géoréférencer** une image (un raster) c’est lui affecter des coordonnées géographiques et rectifier une image c’est la corriger de manière géométrique (déformation, étirement, ...) 

 

QGIS ne distingue pas les deux traitements, ils sont inclus dans l’outil Géoréférenceur. 

 

Dorénavant, nous ne parlerons plus que de géoréférencement. C’est la méthode de transformation qui permettra ou non de rectifier/corriger l’image.

Pour réussir cette transformation spatiale, on s’appuie sur le changement de coordonnées d’un petit nombre d'objets : les **points de calage** dont on a repéré la position dans l’image à corriger et dans l’image de référence.

 

Le géoréférencement est une autre manière très utilisée pour créer des entités à partir d’une image géoréférencée.

Il arrive que les contours de quelques faits n’aient pu être pris par le topographe (à défaut les axes des coupes furent pris) mais ont fait l’objet d’un relevé manuel au 20ème ou 50ème ou juste d’un raster (photo pseudo-verticale). Il faut donc les intégrer au plan général. On doit donc faire correspondre les points de calage du topographe aux clous visibles sur les minutes ou la photo pseudo-verticale.

 

Les **points de calage** :

▪ Doivent-être repérés sur les 2 documents (le document à géoréférencer et celui géoréférencé).

▪ Doivent être répartis de façon homogène autour de la zone à géoréférencer / Ne doivent pas être alignés.

▪ Entourent l'objet.

▪ Les clous ne doivent pas être dans les coins de la photo, utiliser si possible un 50mm surtout pas de grand angle (déformation de Barillet)

▪ Si besoin de mosaïquage (plusieurs photos recouvrant un même objet) croquis + flèche Nord sur les photos.

▪ Doivent être identifiés sur le terrain (Numéro vestiges_1 à n) puis identifiable sur la photo/minute.

▪ Doivent se situer sur le même plan (altitude) entre eux ET avec le vestige à géoréférencer !

▪ Si l’usage de photos géoréférencé est systématisé sur une opération, commencer la numérotation avec le point 1 le plus au nord, une flèche nord n'est pas superflue lorsque plusieurs photos sont nécessaires pour une même structure

▪ Dans le cas d’une photo : les 4 points de calage (minimum) doivent encadrer la zone à géoréférencer. 2 points suffisent pour les minutes de terrain.

▪ Faire une photo la plus verticale possible

Pour nous faciliter ce géoréférencement et surtout être certain que la correspondance entre le point de calage sur le raster et le point du topo soit juste, on demande au logiciel d’accrocher le point situé dans un rayon défini autour de l’endroit où on cliquera sur le canevas. Pour cela il faut utiliser l’option **d’accrochage** sur la couche des points topo soit D107698_pts_topo.

 

**Les différentes types de transformation**

▪ **Linéaire** : changement d'échelle et translation sur 2 points (2 pts minimum) ➔ aucune utilité

▪ **Helmert** : mise à l'échelle, translation et rotation (2 pts minimum) ➔ pour les minutes

▪ **Polynomial de degré 1** : mise à l'échelle, translation et rotation (4 pts minimum) ➔ redressement de structures simples et cadastre ancien (avec plus de points)

▪ **Polynomial de degré 2** : déformation du raster, distorsion "courbe" (6 pts minimum) ➔ cadastre ancien : attention car cette formule induit des déformations de type courbe

▪ **Polynomial de degré 3** : déformation du raster (10 pts minimum) ➔ peu utilisée

▪ **TPS** : fait correspondre exactement tous vos points de contrôle mais déformation extrême de l'image entre les points formule pour réaliser du morphing (ou morphose) ➔ inutile ici

▪ **Projective** : déformation du raster (4 pts minimum) ➔ redressement photo sur des surfaces un peu plus importantes

 

#### Les étapes du géoréférencement

●   Repérer sur le canevas de la carte les points de calage, éventuellement afficher les étiquettes

●   Repérer sur la minute ou la photo les points de calage

●   Activer l’accrochage sur la couche dans le canevas de la carte

●   Ouvrir l’[outil de géoréférencement](https://formationsig.gitlab.io/fiches-techniques/raster/02_georef.html) et procéder à la manipulation : menu Raster > georeferencer (aller le cocher dans le Menu Extension, si non présent)

 

***Attention, à partir de la version 3.26, le Géoréférenceur est dans le menu Couche\***

●   Zoomer sur les points de calage

●   Cliquer pour ajouter un premier point

●   La fenêtre propose de saisir les coordonnées (utile quand on géoréférence une copie de plan en pdf) ou “depuis le canevas de la carte”

![img](images/56.png)

●   La fenêtre se réduit pour permettre de cliquer sur les points correspondants dans le canevas de la carte

●   Cliquer sur ce point correspondant

●   Poursuivre la manipulation pour avoir tous les points de calage

●   Cliquer sur les paramètres de transformation ![img](images/57.png)

○   Définir la transformation

○   Vérifier le SCR

○   Nommer le nouveau raster (garder le suffixe _georef et éventuellement ajouter une référence au type de transformation pour qu’une autre personne soit au courant directement des traitements qui ont été faits pour générer cette image, ex. : HLM : Helmert ; P1 : polynomiale 1

○   Cocher "Employer 0 pour la transparence"

○   Cocher "Charger dans Qgis lorsque terminé"

●   Lancer la transformation avec l’icône ![img](images/58.png)

 

### 2.3.2. Numérisation (= dessin)

#### Numérisation simple avec outil natif de QGIS 

Rappel de l’[outil pour dessiner des polygones](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/01_numerisation_1.html) : **ajouter une entité polygonale** ![img](images/59.png)

  

#### Numérisation avancée  

Barre d’outils numérisation avancée : édition et ajout de faits

 ![img](images/60.png) 

Cette barre d’outil est accessible par un clic droit dans la zone gris, puis choix de numérisation avancée. 

Plusieurs outils sont à disposition : 

▪ Pivoter une entité

▪ Simplifier une entité

▪ Ajouter un anneau/effacer un anneau (à partir d’un sommet)

▪ Ajouter une partie/effacer une partie : permet de saisir dans un même enregistrement des entités géographiques distinctes

▪ Remodeler les entités

▪ Séparer des entités

▪ Fusionner des entités

#### Les courbes de Bézier

BezierEditing est une extension de QGIS pour créer des entités en utilisant les courbes de Bézier. 

Pour le détail des options, voir la fiche [correspondante](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/06_bezier_editing.html). 

Il s’agit d’une extension qui apparaît dans une barre d’outils : ![img](images/61.png)

## 

## 2.4. Gestion de la stratigraphie simple 

### 2.4.1. Avec l’option éviter le chevauchement

dans les options d’accrochage

### 2.4.2. Contrôler l’ordre de rendu des entités

 

●   Créer un champ pour la gestion

○   Dans la couche des vestiges, créer un champ *ordre* (type nombre entier, longueur 1, valeur pour tout le monde "0")

○   dessiner les nouvelles entités et leur attribuer la valeur "-1", "0" ou "1" en fonction des superpositions selon l'ordre : -1 en dessous, puis 0, puis 1

●   Adapter la symbologie

○   Dans les propriétés de la couche, onglet *Symbologie - Rendu de couche*, cocher *Contrôle de l'ordre de rendu des entités*

○   Cliquer sur l'icône à droite pour ouvrir la boîte de dialogue

![img](images/62.png)

![img](images/63.png) 

○   Sélectionner le champ *ordre*, puis choisir *Ascendant*.

![img](images/65.png )



## 2.5. Statistiques de base

Une fois que le plan de masse a été complété, on va pouvoir faire quelques opérations pour gérer notre diag : faire des calculs (via la calculatrice de champs ![img](images/66.png) et avoir des statistiques (outil ![img](images/67.png)).

*Objectif : Calcul du taux d’ouverture du diagnostic*

●   Obtenir la **surface de l’emprise**

○   Soit en allant voir dans la table attributaire

○   Soit en utilisant l’outil Identifier les résultats (dans le volet Déroulé)

○   Soit en utilisant la calculatrice de champs 

○   Soit en utilisant l’outil Ajouter les attributs de géométrie (dans Vecteur > Outil de géométrie > Ajouter les attributs de géométrie)

●   Récupérer les informations de **surface** dans la couche ouverture

○   Ouvrir la table attributaire

○   Cliquer sur la calculatrice de champ

○   Créer un nouveau champ “surface” de type nombre entier

○   Dans le panneau central, aller à Géométrie et choisir $area

○   Cliquer sur ok

●   Utiliser l’outil **Statistiques**

○   Isoler d’abord les valeurs ‘tranchée’ 

■   Soit par le filtre de champ

■   Soit en utilisant la sélection par expression ![img](images/68.png) avec la formule ‘‘typouvert’’ LIKE ‘tranchée’

○   Cliquer sur l’outil Montrer le résumé statistique <img src="images/69.png" alt="img" style="zoom:67%;" />

○   Choisir la couche à interroger, puis le champ à interroger

○   Cliquer sur “entités sélectionnées uniquement” en bas de la fenêtre 

○   Noter la valeur de la somme des surfaces

○   Effectuer le produit en croix



# 3. Manipuler et interroger la table attributaire

## 3.1. La jointure attributaire

*Les infos sont aussi disponibles dans la* [*fiche technique*](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html)*.* 

 

On fait une jointure lorsque l’on souhaite ajouter à une couche vectorielle des informations issues d’un tableau (exemple tableau d’inventaire). Pour cela, il faut identifier dans chaque couche le champ commun : en général en archéologie, il s’agit du numéro de vestige.

Le nom du champ n’a pas besoin d’être identique.

 

### 3.1.1. Jointure attributaire d’un suivi de terrain

●   Charger le tableau D107968_suivi_terrain.ods

●   Ouvrir la table attributaire du tableau

○   Vérifier l’encodage

○   Identifier le champ commun

●   Dans les propriétés de la couche vectorielle qui va recevoir les données (ici D1047698_poly), cliquer sur l’onglet Jointure

●   Cliquer sur le bouton ![img](images/70.png)pour ajouter une jointure 

○   La fenêtre suivante s’affiche

![img](images/71.png)

○   Choisir la couche à joindre

○   Sélectionner les champs qui contiennent le numéro de vestige

○   Cocher “Personnaliser le préfixe des champs” et remplacer par “_”

○   Ouvrir la table attributaire de D107698_poly pour vérifier que la jointure est réalisée.

Vous constatez que les champs ont été ajoutés, et qu’ils comportent bien la mention “_” au début (plus pratique à la lecture et pour les interrogations).

 

Avec cette jointure, on peut déjà interroger la couche poly : 

 

​      Quels sont les faits fouillés ? Où sont-ils ?

​      Quels sont les faits fouillés qui ont fait l’objet d’un prélèvement ? (requête en cascade avec filtrer la sélection courante)

​      Quels sont les faits fouillés, où il y a eu un prélèvement, et qui ont été pris en photo ?

 

### 3.1.2. Jointure attributaire d’un tableau d’inventaire de faits

 

●   Charger le tableau D107698_fait.csv avec l’outil “Ajouter une couche de texte délimité” (dans etude > etude_RO)

●   Ouvrir la table attributaire du tableau

○   Vérifier l’encodage

○   Identifier le champ commun

●   Dans les propriétés de la couche vectorielle qui va recevoir les données (ici D1047698_poly), cliquer sur l’onglet Jointure

●   Cliquer sur le bouton <img src="images/icone_plus.png" alt="img" style="zoom:67%;" /> pour ajouter une jointure 

○   La fenêtre suivante s’affiche

![img](images/72.png)

 

○   Choisir la couche à joindre

○   Sélectionner les champs qui contiennent le numéro de vestige

○   Cocher “Personnaliser le préfixe des champs” et remplacer par “inv_”

○   Ouvrir la table attributaire de D107698_poly pour vérifier que la jointure est réalisée.

Vous constatez que les champs ont été ajoutés, et qu’ils comportent bien la mention “inv_” au début (plus pratique à la lecture et pour les interrogations).

●   Pérenniser la jointure (l’enregistrer en dur) 

○   Clic droit sur la couche > Exporter > Sauvegarder les entités-sous en D107698_poly_inv.shp

##  

## 3.2. Mettre à jour les données

### 3.2.1. Avec la barre d’outils de la table attributaire

![img](images/73.png)

En mode édition, choisir le champ à modifier et saisir la nouvelle donnée. 

Attention, pensez aux apostrophes si vous notez du texte !

 

Vous pouvez n’éditer qu’une partie des données avec une sélection préalable et en cliquant sur “Mettre à jour la sélection”.

### 3.2.2. Avec la calculatrice de champ

Objectif : Renseigner le champ “interpret” avec les données plus récentes du champ “inv_type”. 

 

Pour ne remplir que les entités où le champ “inv_type” a une information, il faut d’abord les sélectionner :

●   Sélection par expression ![img](images/icone_epsilon.png) puis écrire “inv_type” IS NOT NULL (renvoie les lignes non vides)

●   Mettre à jour le champ “interpret”

○   Ne prendre en compte que les entités sélectionnées (en haut à gauche)

○   Sélectionner le champ à mettre à jour (en haut à droite)

○   Dans le panneau central, aller chercher dans le sous-menu “Champs et valeurs” le champ “inv_type” et faire un double clic

○   La valeur “inv_type” est écrite dans la console à gauche

○   Cliquer sur *OK

![img](images/75.png) 

 

De la même manière, mettre la valeur ‘Contemporain’ dans le champ ‘‘invF_chrono’’ pour les fossés Napoléoniens (‘‘invF_chron’’ LIKE ‘Napoleonien’)

●   Effectuer une sélection au préalable des enregistrements à mettre à jour

![img](images/76.png)

 

●   Ne mettre à jour que les entités sélectionnées

●   Choisir le champ à mettre à jour dans le menu déroulant à droite

●   Vérifier que la valeur ‘Contemporain’ n’existe pas déjà dans ce champ en cliquant sur Toutes à droite

●   Faire un double clic sur ‘Contemporain’

●   Cliquer sur ok 

●   Enregistrer et quitter le mode édition

<img src="images/77.png" alt="img" style="zoom:67%;" />



##  

## 3.3. Interroger les données

### 3.3.1. Interrogation simple : filtre de champ + barre de recherche de QGIS

 La manière la plus simple de rechercher la réponse à une question du type : “quels sont les faits fouillés ?” consiste à cliquer sur l’en-tête de colonne adéquate pour trier les valeurs par ordre alphabétique puis de sélectionner les lignes afin de zoomer éventuellement.

 

L’utilisation du filtre de champ permet de chercher une information en filtrant d’abord la table attributaire. 

Exemple : filtrer sur le champ inv_fouille : oui (R = 12)

Puis sélection des entités et zoom si où souhaite savoir où elles sont.

![img](images/78.png)



### 3.3.2. Interrogation avancée : formulaire et filtres

L’utilisation de l'icône filtre les entités ![img](images/icone_filtre.png)dans la table attributaire permet de chercher une information en la sélectionnant et en donnant la liste des entités concernées.

 

Cliquez sur “Sélectionner les entités” : l’interface de la fenêtre se modifie et à gauche, vous obtenez la liste des entités concernées par la question, et en haut le résultat. Cliquer ensuite sur la loupe pour savoir où ils sont . Exemple : inv_fouille : oui + inv_prelevement : oui (R = 3)

![img](images/80.png)

 

Combinaison de critères : filtre ![img](images/icone_filtre.png) ou filtre avancé ![img](images/icone_epsilon.png) (sélection par expression)

Filtrer les vestiges qui ont été **fouillés** et ont fait l’objet d’un **prélèvement + photographiés** (R = 2)

![img](![img](images/81.png)

ou filtre avancé (expression), une fenêtre apparaît où l’on va pouvoir écrire une petite requête :

![img](images/82.png)

 

Pour voir le résultat, il faut **ouvrir la table attributaire** et lire en haut le nombre d’entités sélectionnées. Nous vous conseillons d’ouvrir d’abord la table attributaire, d'utiliser **l’icône de sélection accessible depuis la table**, et de faire migrer les « entités au sommet » pour voir votre résultat dès que vous aurez formulé les requêtes.

 

Requêtes en cascade : poser les requêtes les unes après les autres, en faisant « Selectionner » pour la première, puis ![img](images/83.png) pour les autres.

Cela permet d’avoir plusieurs niveaux de réponse (résultats), et de vérifier//contrôler également ces différentes étapes (regarder les résultats dans la table attributaire).

#### option : le langage SQL

 

Le SQL : Une requête se construit comme tel avec une syntaxe particulière :

▪ j’indique au logiciel dans quel champ il doit trouver la valeur recherchée : "champ"

▪ j’indique au logiciel comment il doit trouver cette valeur : opérateur de comparaison

▪ j’indique au logiciel quelle valeur je recherche : ‘valeur’

En somme, je rédige une phrase : "champ" OPERATEUR DE COMPARAISON ‘valeur’

![img](images/84.png) 

 

Attention à la différence entre 0 (zéro) et la valeur nulle (NULL)

 

Les caractères JOKER : ils impliquent l’utilisation des opérateurs LIKE et ILIKE (surtout pas « = »)

▪ % remplace n’importe quel nombre de caractères

▪ _ remplace un seul caractère

 

Lorsque je souhaite faire une requête répondant à plusieurs conditions, je vais devoir utiliser un opérateur logique pour lier les conditions entre elles. Ils existent deux types d’opérateurs logiques :

AND : il vérifie que la condition 1 et la condition 2 sont vraies

OR : il vérifie que la condition 1 ou la condition 2 est vraie.

 

Il faut insister sur la syntaxe avec les opérateurs logiques et écrire ceci au tableau

![img](images/85.png)

 

#### Mise en oeuvre de la requête attributaire 

Utilisation du langage de requête SQL depuis la table attributaire via “sélectionner les entités” ![img](images/icone_epsilon.png) en utilisant une expression. Interrogation sur D107698_poly_invF.

Le résultat des requêtes n’est pas donné dans la fenêtre de sélection, mais s’affichera bien sûr dans la table attributaire correspondante (la sélection est le résultat de la requête).

Aussi, on vous conseille d’ouvrir d’abord la table attributaire de la couche que vous interrogez, puis d’ouvrir la fenêtre de sélection pour que les résultats s’affichent au 2nd plan dans la table.

 

Sélection des trous de poteau : R= 5

![img](images/86.png)

Sélection des trous de poteau de la ‘Protohistoire’ - R = 3

 

**Quand la question porte sur des champs différents** :

\- soit on tape une phrase SQL d’un seul tenant :

‘‘invF_type’’ LIKE ‘trou de poteau’ AND (‘‘invF_chrono’’ LIKE ‘Protohistoire’ OR ‘invF_chrono’’ LIKE ‘Age du Bronze’ OR ‘invF_chrono’’ LIKE ‘Age du Bronze ancien’)

 

\- soit on fait ce qu’on appelle une requête en cascade, ce qui permet d’obtenir des quotas les uns après les autres, et de vérifier ces résultats (tous les faits datés de la Protohistoire ; puis isoler les trous de poteau) :

‘‘invF_type’’ LIKE ‘trou de poteau’ : cliquez sur Sélection

**puis** ‘‘invF_chrono’’ IN ( ‘Age du Bronze’, ‘Age du Bronze ancien’, ‘Protohistoire’) : !! ne pas cliquez sur Sélection, mais sur la flèche pour accéder à Sélectionner depuis la sélection

Vérifier et accéder au résultat dans la table attributaire.

![img](images/87.png)

 

Sélection des faits ayant du mobilier - R = 17

IS NOT NULL : sous-entendu “qui comporte une valeur”.

![img](images/88.png)

 

Sélection des vestiges ayant livré de la céramique - R = 8

Quand il y a plusieurs valeurs dans un même champ qui comportent un vocable recherché (ici, céramique), on utilise alors l’opérateur IN (ça évite de répéter plusieurs fois ‘‘invF_mobil’’ LIKE ‘céramique, lithique, faune’ OR ‘‘invF_mobil’’ LIKE ‘céramique, lithique’ OR ‘‘invF_mobil’’ LIKE ‘céramique, charbons de bois’ OR ‘‘invF_mobil’’ LIKE ‘céramique’

On peut aussi tout simplement utiliser le caractère joker % : ‘‘invF_mobil’’ LIKE ‘%céramique%’

![img](images/89.png)

 

Pour créer un champ et faire en sorte d’utiliser cette sélection préalable, on utilise la calculatrice de champs, en n’oubliant pas de cocher “pour les entités sélectionnées” :

![img](images/90.png)

On pourrait faire cela pour tous les types de mobilier (champ présence 1 / absence 0) ; donc poser par exemple les questions :

‘‘invF_mobil’’ LIKE ‘%lithique%’ puis aller dans la Calculatrice de champs et créer un champ ‘‘Lithique’’ en mettant la valeur 1

‘‘invF_mobil’’ LIKE ‘%faune%’ puis aller dans la Calculatrice de champs et créer un champ ‘Faune’’ en mettant la valeur 1

‘‘invF_mobil’’ LIKE ‘%charbons de bois%’ puis aller dans la Calculatrice de champs et créer un champ “Charbon”en mettant la valeur 1

!! : Pour mettre 0 aux entités non concernées, il faut inverser la sélection ![img](images/icone_inverser_selection.png)puis retourner à la calculatrice de champs, et mettre à jour le champ concerné avec la valeur 0.

 

Recours à l’outil « Montrer le résumé statistique » ( !! : fonctionne que pour les champs en format numérique ; ne pourra pas faire de somme ou de moyenne sur un champ texte).

 

Quelle est la largeur moyenne des fossés ("interpret" LIKE 'fossé' = 46 fossés) ? - R = 0.9

<img src="images/91.png" alt="img" style="zoom:67%;" />

 

Quelle est la profondeur moyenne des trous de poteau (= 5 trous de poteau) ? - R = 0.18

![img](images/92.png) 



## 3.4. Les cercles proportionnels

On va vouloir mettre des cercles proportionnels en fonction des NR (champ numérique).

Mettre des cercles proportionnels en fonction du champ ‘‘NR’’ 

Dans Propriétés > Symbologie > Symbole unique 

On va faire varier la taille, en fonction du champ ‘‘NR’’ (les cercles seront vraiment proportionnels les uns aux autres, en respectant les bornes min-max du jeu de données).

 ![img](images/93.png) 

<img src="images/94.png" alt="img" style="zoom:67%;" />

Pour être certains que des cercles plus gros ne cachent pas des cercles plus petits, cochez-en bas de la fenêtre Style, “Contrôle de l’ordre de rendu des entités”, et à droite, paramétrez en indiquant “Descendant” pour le champ ici, “inv_NR” (pour que les plus gros soient derrière).

<img src="images/95.png" alt="img" style="zoom:67%;" /> 

<img src="images/96.png" alt="img" style="zoom:67%;" /> 

Pour ajouter la légende, il faut aller la chercher dans le panneau Propriétés > Symbologie > Avancé > Légende définie par la taille des données.

![img](images/97.png) 

Puis choisir les paramètres ; après avoir cliqué sur “OK”, la légende est disponible dans le panneau des couches.

![img](images/98.png) 

On peut aussi associer la typologie (champ ‘‘interpret’’) et les cercles proportionnels (‘‘NR’’).

●   Catégoriser sur le champ “interpret”

<img src="images/99.png" alt="img" style="zoom:80%;" />

 

Ainsi, vous obtiendrez des cercles proportionnels en fonction de l’ensemble du jeu de données, par catégorie ; ce qui est correct dans une optique de comparaison. Ou vous pouvez alors cocher que le type de données qui vous intéresse et faire ainsi plusieurs cartes comparatives.

On peut également y ajouter le décompte des entités :

<img src="images/100.png" alt="img" style="zoom:80%;" /> 

#  

# 4. Interroger les relations spatiales            

## 4.1. Vérifier et corriger les erreurs de géométrie : le volet spatial 

#### 4.1.1. Notions de topologie et de géométrie

 

La **topologie** étudie les déformations géométriques des objets, et décrit les **relations** entre les points-lignes-polygones. Les règles de topologie permettent de vérifier les relations spatiales entre les entités : elles peuvent avoir une valeur descriptive (état : comment se touchent / superposent / recouvrent les entités > la règle à vérifier dépend alors de votre problématique) mais aussi (et surtout ici) constituer un vérificateur de topologie et de géométrie.

 

Les **erreurs de topologie** peuvent être dues à des manipulations ou être générées par les logiciels lors d’opérations spatiales. Aussi, avant toute opération spatiale (jointure ou analyse ou étude statistique), il faut vérifier la topologie et la géométrie de toutes vos couches vectorielles.

​     

La **géométrie** constitue l’entité physique de toute entité. Si une entité est “mal” dessinée (présentant des segments qui s’entre-croisent ou des doublons spatiaux, c’est-à-dire des deux points d’ancrage l’un sur l’autre), le logiciel ne la “comprendra” pas et ne pourra exécuter les calculs ; si on lance un calcul, ça le fera bugger.

A noter que QGIS crée des croix vertes lorsque croisement ou doublons spatiaux au cours de la numérisation : 

![img](images/101.png)

 

Et que s’il reste des erreurs de géométrie, les opérations spatiales (jointures, requêtes) ne pourront fonctionner :

<img src="images/102.png" alt="img" style="zoom:80%;" />



### 4.1.2. Outil “vérifier la validité”          

Pour corriger les erreurs, il existe différents outils dans QGIS. Pour ce niveau d’initiation, le plus usuel et didactique est l’outil Vérifier la validité.

Menu Vecteur → Outils de géométrie → Vérifier la validité

 

Outil très pratique pour analyser les erreurs de géométrie et les pointer (crée 3 couches temporaires, dont un de point où se situent les erreurs).

![img](images/103.png)

Vous pouvez ouvrir la table attributaire pour lire de quel type d’erreur il s’agit.

Et zoomer dessus au moyen de l’outil loupe ![img](images/icone_loupe.png)

L’outil de noeuds ![img](images/icone_noeuds.png) est la solution la plus simple à utiliser dans le cas où les nœuds en double sont peu nombreux. Sinon, boîte de traitement, outil « supprimer les sommets en double » (identifie et supprimer les doublons de nœuds).

**Attention** de bien modifier le vecteur d’origine (et non pas le vecteur Sortie invalide qui sert juste de repère.

●   Passer en mode Édition

●   Zoomer sur les erreurs les unes après les autres 

●   Éventuellement utiliser l’accrochage

●   Au moyen de l’outil noeud, supprimer ou déplacer les points d’ancrage

●   Enregistrer les corrections faites

##  

## 4.2. Option : La requête spatiale - interrogation sur la localisation

Une requête spatiale est une **interrogation portant sur la géométrie et la position des entités** d'une ou plusieurs couches et permettant de sélectionner des entités en fonction des entités d'une autre couche, selon donc sa localisation. Une telle requête nécessite l'utilisation d'opérateurs de sélection géographique (intersection, inclusion, contiguïté, proximité...).

Menu Vecteur > Outils de recherche > Sélection par localisation

 

*Objectif : récupérer les faits du diagnostic compris dans les zones de fouille → Requête spatiale entre D107698_poly_inv et F111688_ouverture*

●   Importer F111688_ouverture (qui contient les limites du décapage de la fouille)

●   Faire la sélection spatiale (attention, par défaut, QGIS retient la sélection sur les couches, donc bien désélectionner ![img](images/icone_deselection.png)avant toute interrogation - sauf si cela est souhaité)

 <img src="images/104.png" alt="img" style="zoom:67%;" />

 

●   Isoler la sélection dans un nouveau vecteur D107698_polyDiag_fouille (Exporter > Sauvegarder les entités sélectionnées sous dans le dossier F111688_Ploenour > donnee_spatiale > vecteur_ope).

<img src="images/105.png" alt="img" style="zoom:80%;" />

 

*Objectif : combien de faits de la fouille ont été vus lors du diagnostic ? → Requête spatiale entre D107698_poly_inv et F111688_poly*

![img](images/106.png)

*Comment pourrait-on effectuer la correspondance et récupérer les numéros ?*

\>> Par une jointure spatiale et récupérer dans F111688_poly, le champ ‘‘numpoly’’ de la couche D107698_poly_inv.

Comme les tables attributaires ont la même structure (cf. CAVIAR) puisqu’il s’agit de couches devant recevoir les vestiges archéologiques, QGIS renomme automatiquement en “..._1” les champs venant de la jointure spatiale (donc ceux de D107698_poly_inv).

A vous de les renommer dans les Propriétés de F111688_poly, via l’onglet Champs :

![img](images/107.png)

![img](images/108.png)

 

*Objectif : Quels faits ont été ‘‘testés’’ (traversés par un axe de coupe) ?* 

![img](images/109.png) 

 

Créer un champ ‘‘fouille’’ et mettre la valeur 1 (pour oui) en créant un champ de type Entier et de longueur 1 ; puis mettre à jour ce champ en saisissant la valeur 0 (pour non) :

![img](images/110.png)

##  

## 4.3. Option : La jointure spatiale : compléter les données  

De même que la jointure attributaire permet de mettre en correspondance des tableaux via un champ commun, la jointure spatiale permet également d’ajouter des informations à une table attributaire mais en utilisant les relations spatiales entre les couches.

En effet, on a recours à la jointure spatiale quand il n’y a pas de champ commun entre les tables attributaires. Élément commun pour que jointure il y ait : la localisation !!

Exemple : Il existe une relation spatiale entre les points contenus dans D107698_point (les isolats) et la couche du cadastre car les points sont contenus dans une parcelle.

Menu Vecteur → Outils de gestion de données → **Joindre les attributs par localisation**.

*Objectif : récupérer le numéro et la section des parcelles, sur la table attributaire des points, et ce pour actualiser l’inventaire pour le SRA (liste des isolats par parcelle cadastrale).*

●   Jointure spatiale entre D107698_iso_inv.shp et Cadastre.shp (importer ce vecteur). Option : **Prendre uniquement les attributs de la première entité localisée correspondance unique** (il n’y a qu’1 parcelle derrière chaque point).

●   Vous pouvez ne sélectionner que les champs qui vous intéressent (sinon les supprimer ultérieurement). Vous pouvez personnaliser le préfixe comme dans les jointures attributaires pour savoir quels sont les champs qui proviennent du vecteur joint « cad_ ».

●   Ne pas cocher « Supprimer les enregistrements qui ne peuvent être joints » pour tout conserver.

<img src="images/111.png" alt="img" style="zoom:67%;" />

●   Exporter la couche temporaire sous iso_InvCad.shp et l’enregistrer dans le dossier vecteur_analyse.

●   Ouvrir la table attributaire pour voir les champs issus du cadastre

 

Attention, par défaut, dès que l’on fait “ du spatial “ dans QGIS, les nouvelles couches ajoutées prennent le nom de la fonction et non celui qu’on lui a indiqué avant. Mais si vous passez le curseur sur le nom du vecteur “Couche jointe” ou si vous allez dans les Propriétés > onglet Information, vous verrez bien qu’il est bien stocké là où vous l’avez enregistré et qu’ici, il est bien nommé “point_InvCad”.

![img](images/112.png)

●   Supprimer les champs en trop (‘‘inspire_id’’ et ‘‘département’’), si vous n’aviez pas sélectionné les champs auparavant.

●   Créer un inventaire dans Open Office ou Excel (Copier dans QGIS → Coller dans 1 tableur : et voilà !)

*Objectif : associer le numéro de tranchée à chaque vestige dans lequel il se trouve*

Mais attention, comme dans la couche D107968_ouverture il y aussi les sondages, il faut faire une sélection attributaire préalable (‘‘typouvert’’ LIKE ‘tranchée’) pour ensuite faire la jointure spatiale << QGIS retient qu’il y a une sélection préalable même s’il ne le signifie pas.

●   Jointure spatiale entre D107968_poly_inv et D107968_ouverture

●   Prendre les attributs de la première entité localisée (il n’y a qu’une tranchée derrière les faits)

●   Exporter cette couche en Poly_Ouv.shp

●   Supprimer les champs inutiles (les infos des ouvertures, sauf numouvert)

 

*Objectif : récupérer les informations d’altitude (minimum, maximum et moyenne) de chaque vestige* 

Outil **Joindre les attributs par localisation (résumé)** issu de la boîte à outils de Traitement : possible sur les champs de type numérique

Jointure spatiale entre D107698_poly_inv.shp et D107698_ptsTopo.shp

●   Boite à outil de traitement > Outils généraux pour les vecteurs > joindre les attributs par localisation (résumé) 

●   Choisir le champ sur lequel les opérations mathématiques seront faites (ALT)

●   choisir les calculs mathématiques à effectuer (count : nombre de points concernés par fait ; min ; max ; mean)

●   Nommer le nouveau vecteur Poly_Alt.shp (dans vecteur_analyse)

![img](images/113.png)

●   Ouvrir la table pour visualiser les colonnes créées

Quelle est l’altitude moyenne du fait 65 ? Filtre de champs : numpoly = 65 - R = 52,107 (info dans le nouveau champ ‘‘ALT_mean’).

#  

# 5. Archivage et CAVIAR

Zoomer sur l’emprise du diag (ou faire sur un diag local)

Connexion à CAVIAR dans QGIS ![img](images/114.png)

 

<img src="images/115.png" alt="img" style="zoom:67%;" />

Importer les emprises, les ouvertures.

Ouvrir la table attributaire du SHP emprise : si le lien avec Dolia n’est pas automatique, le faire pour

activer le lien vers le rapport. Paramétrer l’action via les propriétés de la couche :

![img](images/116.png)

 

#  

# 6. Sémiologie et cartographie

## 6.1. La sémiologie graphique

## 6.2. Mise en page 

### 6.2.1. Présentation

Attention : ce module ne permet de faire « que » de la mise en page, ce qui implique que le choix des couches à afficher, des styles et des étiquettes se fait dans le Canevas du Projet. Le lien se fera ensuite dans ce module.

Projet > Nouvelle mise en page (ou Gestionnaire de mises en page s'il y a plusieurs mises en pages déjà disponibles ou en cours)

**Rappel du principe**

●   Ajout des éléments au fur et à mesure en cliquant sur les icônes correspondantes (ou Menu Ajouter un élément)

●   Modification des éléments (ce sont des objets insérés) à l’aide des onglets situés à droite (les onglets s’adaptent lorsque l’on sélectionne l’objet à modifier)

**Paramètres de la composition** : pour définir les propriétés de la page vierge (dimensions, orientation portrait/paysage...) >> faire un clic-droit sur la page > Propriétés de la page

Onglet **Mise en page** : vous pouvez ajouter une grille (guide de mise en page comme dans InDesign), ou paramétrer les exports

 

**Outils pour se Déplacer** : bien maîtriser la différence et la complémentarité entre la Sélection d’un élément pour le déplacer dans la composition une fois importé, et le fait de déplacer le contenu de l’élément Carte (pour recentrer la carte et zoomer/dézoomer)

**Onglet Propriétés de l’objet** : il s’actualise en fonction de l’élément ajouté

 

#### Ajouter une carte

Ajouter la carte à l’aide du bouton « ajouter une nouvelle carte » ![img](images/117.png)

Déplacer ou redimensionner la carte avec l’outil ![img](images/118.png)

Se déplacer à l’intérieur de carte ![img](images/119.png)

 

●   Paramétrer les dimensions de la page selon les normes Inrap des figures réglementaires du RFO (255 x 170)

●   paramétrer l’échelle d’affichage des données

●   position et taille, cadre : affichage ou non du cadre etc...

●   afficher la grille (graticule) : permet d’afficher les grilles, d’en définir le type, l’intervalle en X et en Y, l’affichage des annotations (coordonnées)..

Vous pouvez commencer par ajouter l’élément échelle graphique pour avoir une idée de l’intervalle de distance le plus approprié à indiquer pour votre carroyage (grille).

#### Ajouter une échelle graphique

#### Ajouter une étiquette

Pour ajouter du texte supplémentaire (titre, sources, auteur...)

#### Ajouter une image

Possibilité de charger une image pré-définie (.tif, .jpg...)

#### Ajouter une flèche du nord

Possibilité d’ajouter ses propres flèches au format svg

#### Ajouter une légende

objets de légende : permet de modifier l’ordre des couches, le texte (des couches et des symboles associés) qui, par défaut, correspondent à ce qui est affiché dans la vue. Pour cela, voir les boutons situés en bas

#### Ajouter une table d’attributs

●   possibilité d’ajouter une des tables de la vue (à sélectionner à partir de « couches »)

●   possibilité de sélectionner les attributs à afficher à partir de la couche (« Attributs »)

#### Ajouter une carte de localisation/aperçu

On peut ajouter une seconde carte (à partir de la même vue) affichée avec une échelle différente de l’échelle de la vue principale.

●   Onglet propriétés de l’objet

●   Aperçu : définir le cadre de l’aperçu (si plusieurs cartes) ; le style (type de cadre, remplissage, bordure) ; ne pas hésiter à rafraîchir si besoin

!! : bien verrouiller les couches et les styles de la première carte (propriété) : ainsi lorsque vous ajouterez une autre carte, la première ne sera pas impactée par le changement de couches et de styles qui sera effectué dans le projet.

Attention ! pour accéder aux paramètres de l’objet ajouté sur la carte à droite, il faut d’abord le sélectionner sur la carte ou via l’onglet Eléments.

On peut renommer les cartes dans l’onglet Mise en page

#### Onglet Elément

Vous pouvez renommer les éléments ajoutés (meilleure compréhension) et les verrouiller dans la mise en page, ou les masquer, ou accéder à leurs propriétés

#### Menu Mise en Page

Export de la carte possible en format : 

●   Image (.JPEG, .TIF ; …) ;

●   SVG (format vectoriel) ;

●   PDF (décocher : Impression raster si QGIS pose la question lors de l’export ; ou aller dans l’onglet Composition et décocher Impression Raster).

 

### 6.2.2. Exercice de mise en page

On souhaite obtenir la carte suivante : 

![img](images/120.png)

 Procédure : 

#### Préparer les couches dans le canevas de la carte

●   Dans le canevas du projet en cours, **ajouter les couches** :

○   F111688_ouverture.shp

○   F111688_poly.shp

○   le cadastre vecteur du répertoire vecteur_georef > vecteur_fonds_carto

●   **Symbologie**

○   F111688_ouverture : 

■   type de symbole : bordure ligne simple

■   couleur : noir

■   épaisseur de trait : 0.2 mm

■   style de ligne : ligne tiret-point

○   F111688_poly : 

​		type de symbole : remplissage simple

​		couleur de remplissage : gris RGB (150, 150, 150)

■   couleur de trait (bordure) : noir

■   style de trait : ligne continue

■   épaisseur de trait : 0.15 mm

○   cadastre : 

■   type de symbole : bordure ligne simple

■   couleur RGB (100, 50, 0)

■   épaisseur du trait : 0.15 mm

■   étiquettes : champ “numéro”, police Arial 6 pt, tampon blanc 0.5 mm

 

#### Dans le module de mise en page

●   Projet > Nouvelle mise en page

●   Titre de la mise en page : F111688_interprétation 

●   Paramétrer la taille de la feuille

​	Clic droit sur la page blanche

​	Propriétés de la page : hauteur 240 mm ; largeur 170 mm

●   Ajouter une **carte** 1

●   La renommer dans l’onglet Elément en “Carte générale”

●   **Propriétés de la carte générale** 

​	Echelle à 2000 : cliquer sur la boite à droite du champ de l’échelle > Editer > Saisir 2000 dans le panneau de gauche

​	Position et taille : X = 0, largeur 117.5 mm ; hauteur 100 mm

​	Ajouter un cadre

​	Grille > Ajouter une grille

■   type de grille : croix

■   intervalle X = 200 ; Y = 200

■   largeur des croix : 1 mm

■   style de ligne ; couleur gris RGB (130, 130, 130)

■   largeur de ligne : 0.2 mm

■   cadre de la grille 

●   style du cadre : marqueurs à l’intérieur

●   taille de cadre : 1 mm

●   épaisseur : 0.2 mm

■   Afficher les coordonnées 

●   A droite et en haut dans un cadre ; alignement descendant vertical à droite ; désactiver en bas et à gauche

●   Police : Arial 6 pt

●   Précision des coordonnées : 0     

●   Ajouter une **échelle** 

​	Style : boîte unique 

​	Segments

■   4 à droite

■   largeur fixe 20 m

■   hauteur 1 mm

■   affichage

●   marge de la boite : 1 mm

●   largeur de ligne : 0.2 mm

●   police Arial 6 pt

●   arrière-plan blanc

●   Ajouter une **légende** 

​	Conserver uniquement : cadastre, ouvertures, vestiges avec le filtre

​	Renommer les couches dans le panneau de légende après avoir désactivé la mise à jour automatique 

​	Taille de symbole : 6 mm x 3 mm

​	Espace des symboles : 1 mm

​	Police Arial 6 pt

●   Ajouter une **flèche nord**

 

Une fois la carte souhaitée réalisée, aller dans propriétés de l’objet Carte, cocher **Verrouiller les couches et le style de couche**s, afin de pouvoir réaliser les cartes suivantes dans le canevas sans toucher à l’apparence de notre première carte dans le module de mise en page. 

![img](images/121.png) 

**Retour au canevas de carte (projet principal)** 

●   Ajouter la couche F111688_ensemble (vecteur_georef > vecteur_analyse)

●   Observer la table attributaire pour la catégorisation

●   Catégoriser le style sur la colonne “interpret”, couleurs aléatoires

●   Zoomer sur les greniers

●   Afficher les numéros d’ensemble (de bâtiments) : étiquettes sur “numens”, police Arial 8 pt

●   Cacher les numéros de parcelles 

 

**Retour au module de mise en page** 

●   Ajouter une seconde carte, centrée sur les greniers et le chemin

●   La renommer “Carte grenier”

●   Echelle à 1:500

●   Taille et position : largeur 117 mm, hauteur 100 mm

●   Ajouter un cadre de 0.2 mm

●   Ajouter une échelle

​	4 segments à droite, largeur 3 mm

​	Affichage : marge des étiquettes 1 mm, largeur de ligne 0.2 mm, fond blanc

●   Ajouter un nord

●   Ajouter une légende : conserver ensembles, ouvertures, vestiges

●   Ajouter un aperçu

​	Sélectionner la carte générale

​	Propriétés de l’objet, Aperçu, cliquer sur +, un cadre rouge de localisation apparaît sur la carte initiale 

●   Ajouter une flèche rouge partant de l’aperçu rouge de la carte générale jusqu’à la carte grenier

​	propriétés principales

​	style de ligne : largeur 0.5 mm

​	symbole de flèche

■   couleur de bordure RGB (255, 0, 0)

■   couleur de remplissage de la flèche RGB (255, 0, 0)

■   largeur de bordure de la flèche : 0.2 mm

■   largeur de la tête : 2.2 mm 

●   Ajouter une 3e carte (zoom sur l’habitat) et la renommer Carte Habitat

​	Position et taille : largeur 150 mm, hauteur 68 mm

​	Echelle de la carte : 200e

●   Ajouter une échelle pour la 3e carte

​	4 segments à droite, largeur 2 mm

​	Affichage : marge des étiquettes 1 mm, largeur de ligne 0.2 mm

●   Ajouter un nord à la 3e carte

●   Ajouter une légende à la 3e carte 

●   Dans les propriétés de la 1ere carte (carte générale), ajouter un aperçu sur la carte 3 (carte grenier) 

​	style de cadre : couleur RGB (0, 160, 0)

​	transparence 70 %

●   Ajouter une flèche du même vert que le dernier aperçu, reliant la carte générale à la carte habitat

​	style de ligne : largeur 0.5 mm

​	symbole de flèche 

■   couleur de trait de la flèche RGB (0, 160, 0)

■   couleur de remplissage de la flèche RGB (0, 160, 0)

■   largeur de bordure de la flèche : 0.2 mm

■   largeur de la tête : 2.2 mm

Conseils généraux 

<img src="images/122.png" alt="img" style="zoom:67%;" />

 

![img](images/123.png)

 

Have fun with QGIS !!

 

 

 

 

 

 

 

 

 

 

 

 

